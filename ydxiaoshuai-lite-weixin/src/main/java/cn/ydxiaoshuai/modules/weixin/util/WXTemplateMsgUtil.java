package cn.ydxiaoshuai.modules.weixin.util;


import cn.ydxiaoshuai.modules.weixin.po.TemplateData;
import cn.ydxiaoshuai.modules.weixin.po.WxTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 微信模板消息组装
 * @author 小帅丶
 * @className WXTemplateMsgUtil
 * @Date 2019/12/18-15:09
 **/
public class WXTemplateMsgUtil {
    /**
     * @Description 上级推送模板消息内容组装
     * @Author 小帅丶
     * @Date  2019/12/18 15:07
     * @param template_id 模板ID
     * @param applyDate 申请时间
     * @param applyUserName 申请人
     * @param loanType 贷款类型
     * @return cn.ydxiaoshuai.modules.weixin.po.WxTemplate
     **/
    public static WxTemplate getWxTemplateForLoanApply(String template_id, String applyDate, String applyUserName, String loanType, String firstText, String remarkText){
        WxTemplate wxTemplate = new WxTemplate();
        wxTemplate.setTopcolor("#000000");
        wxTemplate.setTemplate_id(template_id);
        Map<String, TemplateData> map = new HashMap<>();
        //第一行提示
        TemplateData first = new TemplateData();
        first.setColor("#000000");
        first.setValue(firstText);
        map.put("first", first);
        //审核结果 申请时间
        TemplateData applyDateData = new TemplateData();
        applyDateData.setColor("#000000");
        applyDateData.setValue(applyDate);
        map.put("keyword1", applyDateData);
        //申请人
        TemplateData applyUserNameData = new TemplateData();
        applyUserNameData.setColor("#000000");
        applyUserNameData.setValue(applyUserName);
        map.put("keyword2", applyUserNameData);
        //项目名称 贷款类型
        TemplateData loanTypeData = new TemplateData();
        loanTypeData.setColor("#000000");
        loanTypeData.setValue(loanType);
        map.put("keyword3", loanTypeData);
        //抢单备注
        TemplateData remarkLoan = new TemplateData();
        remarkLoan.setColor("#000000");
        remarkLoan.setValue(remarkText);
        map.put("remark",remarkLoan);
        wxTemplate.setData(map);
        return wxTemplate;
    }
}
