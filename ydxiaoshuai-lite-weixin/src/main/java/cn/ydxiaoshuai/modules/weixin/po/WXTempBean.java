package cn.ydxiaoshuai.modules.weixin.po;

import lombok.Data;

/**
 * @Description 模板返回的对象
 * @author 小帅丶
 * @className WXTempBean
 * @Date 2019/11/28-11:06
 **/
@Data
public class WXTempBean extends WXErrorGlobal{
    //模板信息会返回
    private String msgid;
}
