package cn.ydxiaoshuai.modules.weixin.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;
import cn.ydxiaoshuai.common.system.query.QueryGenerator;
import cn.ydxiaoshuai.modules.weixin.entity.WeixinAccount;
import cn.ydxiaoshuai.modules.weixin.service.IWeixinAccountService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 微信公众账号表
 * @Author: 小帅丶
 * @Date:   2020-09-10
 * @Version: V1.0
 */
@Slf4j
@Api(tags="微信公众账号表")
@RestController
@RequestMapping("/weixin/weixinAccount")
public class WeixinAccountController extends JeecgController<WeixinAccount, IWeixinAccountService> {
	@Autowired
	private IWeixinAccountService weixinAccountService;
	
	/**
	 * 分页列表查询
	 *
	 * @param weixinAccount
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "微信公众账号表-分页列表查询")
	@ApiOperation(value="微信公众账号表-分页列表查询", notes="微信公众账号表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(WeixinAccount weixinAccount,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WeixinAccount> queryWrapper = QueryGenerator.initQueryWrapper(weixinAccount, req.getParameterMap());
		Page<WeixinAccount> page = new Page<WeixinAccount>(pageNo, pageSize);
		IPage<WeixinAccount> pageList = weixinAccountService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param weixinAccount
	 * @return
	 */
	@AutoLog(value = "微信公众账号表-添加")
	@ApiOperation(value="微信公众账号表-添加", notes="微信公众账号表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody WeixinAccount weixinAccount) {
		weixinAccountService.save(weixinAccount);
		return Result.ok("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param weixinAccount
	 * @return
	 */
	@AutoLog(value = "微信公众账号表-编辑")
	@ApiOperation(value="微信公众账号表-编辑", notes="微信公众账号表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody WeixinAccount weixinAccount) {
		weixinAccountService.updateById(weixinAccount);
		return Result.ok("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "微信公众账号表-通过id删除")
	@ApiOperation(value="微信公众账号表-通过id删除", notes="微信公众账号表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		weixinAccountService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "微信公众账号表-批量删除")
	@ApiOperation(value="微信公众账号表-批量删除", notes="微信公众账号表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.weixinAccountService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "微信公众账号表-通过id查询")
	@ApiOperation(value="微信公众账号表-通过id查询", notes="微信公众账号表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		WeixinAccount weixinAccount = weixinAccountService.getById(id);
		return Result.ok(weixinAccount);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param weixinAccount
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, WeixinAccount weixinAccount) {
      return super.exportXls(request, weixinAccount, WeixinAccount.class, "微信公众账号表");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, WeixinAccount.class);
  }

}
