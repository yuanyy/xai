package cn.ydxiaoshuai.modules.weixin.po;

import lombok.Data;

import java.util.Map;

/**
 * @Description 微信模板消息对象
 * @author 小帅丶
 * @className WxTemplate
 * @Date 2019/11/27-14:12
 **/
@Data
public class WxTemplate {
    /**
     * 模板消息id
     */
    private String template_id;
    /**
     * 用户openId
     */
    private String touser;
    /**
     * URL置空，则在发送后，点击模板消息会进入一个空白页面（ios），或无法点击（android）
     */
    private String url;
    /**
     * 标题颜色
     */
    private String topcolor;
    /**
     * 标题颜色
     */
    private MiniProgram miniprogram;
    /**
     * 详细内容
     */
    private Map<String,TemplateData> data;

}
