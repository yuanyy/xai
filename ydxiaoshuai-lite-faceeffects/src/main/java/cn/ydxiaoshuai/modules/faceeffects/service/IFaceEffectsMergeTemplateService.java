package cn.ydxiaoshuai.modules.faceeffects.service;

import cn.ydxiaoshuai.modules.faceeffects.entity.FaceEffectsMergeTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 人脸融合模板图表
 * @Author: 小帅丶
 * @Date:   2020-09-04
 * @Version: V1.0
 */
public interface IFaceEffectsMergeTemplateService extends IService<FaceEffectsMergeTemplate> {

}
