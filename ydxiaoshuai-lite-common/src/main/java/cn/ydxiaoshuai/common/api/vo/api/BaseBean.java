package cn.ydxiaoshuai.common.api.vo.api;

import cn.hutool.core.util.IdUtil;
import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 小帅丶
 * @className BaseBean
 * @Description 基类
 * @Date 2019/7/18-14:48
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseBean implements Serializable {
    private static final long timestamps = System.currentTimeMillis();
    /**
     * 返回处理消息
     */
    public String log_id = timestamps+"-"+ IdUtil.fastSimpleUUID();
    /**
     * 时间戳
     */
    public long timestamp = timestamps;
    public Integer code;
    public String msg;
    public String msg_zh;
    /**
     * 作者
     */
    public String author = "小帅丶";

    public BaseBean error(String msg) {
        return error(CommonConstant.SC_INTERNAL_SERVER_ERROR_500, msg);
    }

    private BaseBean error(Integer code, String msg) {
        BaseBean baseBean = new BaseBean();
        baseBean.setCode(code);
        baseBean.setMsg(msg);
        return baseBean;
    }

    public BaseBean(Integer code, String msg, String msg_zh, String author) {
        this.code = code;
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.author = author;
    }
}
