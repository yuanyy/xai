package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className GarbageResponseBean
 * @Description 接口返回的对象
 * @Date 2020/4/26-16:46
 **/
@Data
@NoArgsConstructor
public class RecommendResponseBean {


    private String code;
    private boolean charge;
    private int remain;
    private int remainTimes;
    private int remainSeconds;
    private String msg;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {
        private String status_code;
        private String status_message;
        private List<OutfitsBean> outfits;

        @NoArgsConstructor
        @Data
        public static class OutfitsBean {

            private double score;
            private List<ItemsBean> items;

            @NoArgsConstructor
            @Data
            public static class ItemsBean {

                private String category_id;
                private String category_name;
                private String image_url;
                private String item_name;
            }
        }
    }
}
