package cn.ydxiaoshuai.common.api.vo.config;

import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @author 小帅丶
 * @className IsmIndexNewsResult
 * @Description 首页滚动新闻对象
 * @Date 2020/3/25-11:52
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiteConfigNewsResult extends BaseResponseBean {
    /**
     * 具体参数
     **/
    private List<NewsListData> data;
    @Data
    public static class NewsListData{
        //滚动内容
        private String news_text;
        //滚动内容web跳转地址
        private String news_web_url;
        //滚动内容小程序跳转路由
        private String news_lite_url;
        //排序ID
        private Integer news_sort;
    }
    public LiteConfigNewsResult success(String message, List<NewsListData> data) {
        this.message = message;
        this.code = CommonConstant.SC_OK_200;
        this.data = data;
        return this;
    }
    public LiteConfigNewsResult fail(String message, Integer code) {
        this.message = message;
        this.code = code;
        return this;
    }
    public LiteConfigNewsResult error(String message) {
        this.message = message;
        this.code = CommonConstant.SC_INTERNAL_SERVER_ERROR_500;
        return this;
    }
}
