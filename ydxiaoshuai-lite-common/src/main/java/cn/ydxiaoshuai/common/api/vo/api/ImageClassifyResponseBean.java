package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className ImageClassifyResponseBean
 * @Description 图像识别接口返回
 * @Date 2020/9/24-14:20
 **/
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageClassifyResponseBean extends BaseBean{

    private List<ImageClassifyGeneralResponse.Result> result;
    /**车型识别会有此字段*/
    private String color_result;
    /**车型识别会有此字段*/
    private int result_num;
    /**红酒、地标*/
    private ImageClassifyOtherResponse.ResultBean data;
    /**处理后的图片*/
    private String deal_base64;

    public ImageClassifyResponseBean success(String msg, String msg_zh, String deal_base64) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.deal_base64 = deal_base64;
        return this;
    }

    public ImageClassifyResponseBean success(String msg, String msg_zh, ImageClassifyOtherResponse.ResultBean data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public ImageClassifyResponseBean success(String msg,String msg_zh,List<ImageClassifyGeneralResponse.Result> result,
                                             String color_result,int result_num,String deal_base64) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.result = result;
        this.color_result = color_result;
        this.result_num = result_num;
        this.deal_base64 = deal_base64;
        return this;
    }
    public ImageClassifyResponseBean success(String msg,String msg_zh,List<ImageClassifyGeneralResponse.Result> result,
                                             String color_result,int result_num) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.result = result;
        this.color_result = color_result;
        this.result_num = result_num;
        return this;
    }
    public ImageClassifyResponseBean fail(String msg,String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public ImageClassifyResponseBean error(String msg,String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }

}
