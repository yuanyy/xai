package cn.ydxiaoshuai.modules.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.system.entity.SysDepartRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 部门角色
 * @Author: 小帅丶
 * @Date:   2020-02-12
 * @Version: V1.0
 */
public interface SysDepartRoleMapper extends BaseMapper<SysDepartRole> {

}
