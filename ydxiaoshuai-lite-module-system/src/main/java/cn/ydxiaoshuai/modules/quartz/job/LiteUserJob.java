package cn.ydxiaoshuai.modules.quartz.job;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.ydxiaoshuai.modules.liteuser.service.ILiteUserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author 小帅丶
 * @className LiteUserJob
 * @Description 用户定时任务操作
 * @Date 2020/9/14-15:43
 **/
@Slf4j
public class LiteUserJob implements Job {
    @Autowired
    private ILiteUserInfoService liteUserInfoService;
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //用户注册时长计算
        updateDays();
    }
    /**
     * @Author 小帅丶
     * @Description 根据第一次wxlogin时间计算注册时长
     * @Date  2020/9/14 15:50
     * @return void
     **/
    private void updateDays(){
        log.info("用户天数更新任务执行:{}",DateUtil.format(new Date(), DatePattern.NORM_DATETIME_PATTERN));
        liteUserInfoService.updateUserDays();
    }
}
