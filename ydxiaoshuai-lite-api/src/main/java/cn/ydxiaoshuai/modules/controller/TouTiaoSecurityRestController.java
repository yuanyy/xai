package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.api.vo.api.*;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.common.util.RedisUtil;
import cn.ydxiaoshuai.modules.util.TouTiaoSecurityUtil;
import com.alibaba.fastjson.JSON;
import com.baidu.aip.util.Base64Util;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 小帅丶
 * @className PalmRestController
 * @Description 头条内容安全检测
 * @Date 2020年6月17日16:52:00
 **/
@Controller
@RequestMapping(value = {"/toutiao","/rest/toutiao"})
@Scope("prototype")
@Slf4j
@Api(tags = "头条内容安全检测-API")
public class TouTiaoSecurityRestController extends ApiRestController {
    @Autowired
    private TouTiaoSecurityUtil touTiaoSecurityUtil;
    @Autowired
    private RedisUtil redisUtil;
    /**
     * @Description 内容安全检测接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年6月17日
     **/
    @RequestMapping(value = "/security", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> detectPalm(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{}", requestURI);
        TouTiaoSecurityBean bean = new TouTiaoSecurityBean();
        TouTiaoSecurityRequestBean requestBean = new TouTiaoSecurityRequestBean();
        List<String> targets = new ArrayList<>();
        targets.add("porn");targets.add("politics");targets.add("ad");targets.add("disgusting");
        requestBean.setTargets(targets);
        try {
            //组装请求参数
            List<TouTiaoSecurityRequestBean.TasksBean> tasks = new ArrayList<>();
            TouTiaoSecurityRequestBean.TasksBean tasksBean = new TouTiaoSecurityRequestBean.TasksBean();
            tasksBean.setImage_data(Base64Util.encode(file.getBytes()));
            tasks.add(tasksBean);
            requestBean.setTasks(tasks);
            TouTiaoSecurityResponseBean responseBean = touTiaoSecurityUtil.pictureDetect(requestBean, redisUtil.get("TTAT").toString());
            long count = responseBean.getData().get(0).getPredicts().stream().filter(predictsBean -> predictsBean.getProb()==1).count();
            if(count>0){
                bean.fail("security check fail","图片存在违规",801);
            }else{
                bean.success("success", "图片检查成功");
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("内容安全检测接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{},接口返回内容",timeConsuming, JSON.toJSONString(bean));
        //响应的内容
        return new ResponseEntity<Object>(JSON.toJSONString(bean), httpHeaders, HttpStatus.OK);
    }
}
